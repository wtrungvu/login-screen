
class Validations {

  //TODO: Kiểm tra xem tài khoản UserName đăng nhập có hợp lệ hay ko?
  static bool isValidUserName(String userName){
    return userName != null && 
           userName.length > 5 && 
           userName.contains("@");
  }

  //TODO: Kiểm tra xem mật khẩu Password đăng nhập có hợp lệ hay ko?
  static bool isValidPassword(String passWord){
    return passWord != null &&
           passWord.length > 5;
  }

}