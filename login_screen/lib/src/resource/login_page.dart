import 'package:flutter/material.dart';
import 'package:login_screen/src/blocs/login_bloc.dart';
import 'package:login_screen/src/resource/home_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginBloc bloc = new LoginBloc();

  bool _showPassWord = false;
  // Value
  TextEditingController _userNameController = new TextEditingController();
  TextEditingController _passWordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.fromLTRB(10.0, 150.0, 10.0, 0),
            constraints: BoxConstraints.expand(),
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(
                    child: Text(
                      "Hello\nWelcome Back",
                      style: TextStyle(
                          fontSize: 40.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    child: new Theme(
                        data: new ThemeData(
                          primaryColor: Colors.redAccent,
                          primaryColorDark: Colors.red,
                        ),
                        child: StreamBuilder(
                          stream: bloc.userNameStream,
                          builder: (context, snapshot) => TextField(
                                controller: _userNameController,
                                style: TextStyle(fontSize: 18.0),
                                decoration: InputDecoration(
                                  errorText:
                                      snapshot.hasError ? snapshot.error : null,
                                  labelText: "USERNAME",
                                  hintText: "Enter Your Username",
                                  border: new OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                    ),
                                  ),
                                  suffixIcon: IconButton(
                                      icon: Icon(Icons.cancel),
                                      onPressed: onToggleDeleteUserName),
                                ),
                              ),
                        )),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    child: new Theme(
                        data: new ThemeData(
                          primaryColor: Colors.redAccent,
                          primaryColorDark: Colors.red,
                        ),
                        child: StreamBuilder(
                          stream: bloc.passWordStream,
                          builder: (context, snapshot) => TextField(
                                controller: _passWordController,
                                obscureText: !_showPassWord,
                                style: TextStyle(fontSize: 18.0),
                                decoration: InputDecoration(
                                  errorText:
                                      snapshot.hasError ? snapshot.error : null,
                                  labelText: "PASSWORD",
                                  hintText: "Enter Your Password",
                                  border: new OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                    ),
                                  ),
                                  suffixIcon: IconButton(
                                      icon: Icon(Icons.remove_red_eye),
                                      onPressed: onToggleShowPassWord),
                                ),
                              ),
                        )),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 0),
                  child: SizedBox(
                    width: double.infinity,
                    height: 50.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      color: Colors.redAccent,
                      child: Text(
                        "SIGN IN",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: onSignInClicked,
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 150,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "NEW USER? SIGN UP",
                          style: TextStyle(fontSize: 14.0, color: Colors.grey),
                        ),
                        Text(
                          "FORGOT PASSWORD",
                          style: TextStyle(
                              fontSize: 14.0, color: Colors.redAccent),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  //TODO: Bắt sự kiện khi click vào button 'X'
  void onToggleDeleteUserName() {
    setState(() {
      _userNameController.clear();
    });
  }

  //TODO: Bắt sự kiện khi click vào button 'Eye'
  void onToggleShowPassWord() {
    setState(() {
      _showPassWord = !_showPassWord;
    });
  }

  //TODO: Bắt sự kiện khi click vào button 'SIGN IN'
  void onSignInClicked() {
    if (bloc.isValidInfo(_userNameController.text, _passWordController.text)) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => HomePage(
                    textUserName: _userNameController.text,
                  )));
    }
  }

  //TODO: Chuyển qua màn hình HomePage
  Widget gotoHome(BuildContext context) {
    return HomePage(textUserName: _userNameController.text);
  }
}
