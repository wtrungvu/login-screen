import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final String textUserName;
  //TODO: Hàm khởi tạo - Constructor HomePage
  HomePage({Key key, @required this.textUserName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: ("Home Page"),
      home: Scaffold(
          appBar: AppBar(
            title: Text("Home Page"),
            backgroundColor: Colors.redAccent,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(null),
            ),
          ),
          body: SafeArea(
            child: Container(
              padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
              constraints: BoxConstraints.expand(),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(125.0, 100, 100, 20),
                    child: Container(
                      width: 100,
                      height: 100,
                      padding: EdgeInsets.all(15),
                      child: Image.asset("images/emoji.png"),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.grey,
                          gradient: new LinearGradient(
                              colors: [Colors.red, Colors.blue])),
                    ),
                  ),
                  Center(
                    child: Text(
                      textUserName,
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(75.0, 20.0, 10.0, 0),
                    child: SizedBox(
                      width: 200.0,
                      height: 50.0,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                        color: Colors.redAccent,
                        child: Text(
                          "BACK",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: () => Navigator.of(context).pop(null),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }
}
