import 'dart:async';
import 'package:login_screen/src/validators/validations.dart';

class LoginBloc {

  StreamController _userNameController = new StreamController();
  StreamController _passWordController = new StreamController();

  Stream get userNameStream => _userNameController.stream;
  Stream get passWordStream => _passWordController.stream;

  //TODO:Kiểm tra thông tin hợp lệ?
  bool isValidInfo(String userName, String passWord){

    //TODO: Nếu tài khoản userName ko hợp lệ?
    if(!Validations.isValidUserName(userName)){
      // sink: là một input (Dữ liệu đầu vào) cho Stream
      // Add Error vào Stream userName
      _userNameController.sink.addError("This account is Invalid!");
      return false;
    }
    //TODO: Add OK vào Stream userName
    _userNameController.sink.add("OK");

    //TODO: Nếu mật khẩu passWord ko hợp lệ?
    if(!Validations.isValidPassword(passWord)){
      // Add Error vào Stream passWord
      _passWordController.sink.addError("Password must contain more than 6 character!");
      return false;
    }
    //TODO: Add OK vào Stream passWord
    _passWordController.sink.add("OK");

    //TODO: Thông tin hợp lệ
    return true;

  }

  // Đặc điểm của Stream nó luôn lắng nghe
  //TODO: ta phải tắt, đóng, huỷ nếu ko dùng tới
  void dispose() {
    _userNameController.close();
    _passWordController.close();
  }

}